/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public interface WaveletInterface 
{
    String getName();
    double[] getHighpassFilterParametres();
    double[] getLowpassFilterParametres();
    int getNumberOfParametres();
    int getOrder();
}
