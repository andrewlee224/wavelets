/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public class Daubechies8WV implements WaveletInterface 
{

    public String getName() 
    {
       return "Daubechies-8"; //To change body of generated methods, choose Tools | Templates.
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getLowpassFilterParametres() 
    {
              double[] p = new double[8];
                      p[7] =-0.2303778133089;
                      p[6] =0.71484657055292;
		      p[5] =-0.63088076792986;
                      p[4] =-0.02798376941686;
                      p[3] =0.18703481171909;
		      p[2] =0.03084138183556;
                      p[1] =-0.03288301166689;
		      p[0] =-0.01059740178507;
                      return p;
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getHighpassFilterParametres() 
    {
               double[] p = new double[8];
		      p[0] =0.2303778133089;
                      p[1] =0.71484657055292;
		      p[2] =0.63088076792986;
                      p[3] =-0.02798376941686;
                      p[4] =-0.18703481171909;
		      p[5] =0.03084138183556;
                      p[6] =0.03288301166689;
		      p[7] =-0.01059740178507;
                      return p;
    }
    
    public int getOrder() 
    {
       return 4;
    }

    public int getNumberOfParametres() 
    {
        return 8;
    }

}
