/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public class HaareWV implements WaveletInterface 
{

    public String getName() 
    {
       return "Haar"; //To change body of generated methods, choose Tools | Templates.
    }

    public double[] getLowpassFilterParametres() 
    {
               double[] p = new double[2];
                      p[1] =-1/Math.sqrt(2);
                      p[0] =1/Math.sqrt(2);
                      return p;
    }
    
    public double[] getHighpassFilterParametres() 
    {
               double[] p = new double[2];
                      p[0] =1/Math.sqrt(2);
                      p[1] =1/Math.sqrt(2);
                      return p;
    }

    public int getOrder() 
    {
       return 1;
    }

    public int getNumberOfParametres() 
    {
        return 2;
    }

}
