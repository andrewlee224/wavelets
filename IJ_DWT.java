package DWT;

import ij.plugin.frame.PlugInFrame;
import javax.swing.JFrame;

public class IJ_DWT extends PlugInFrame {

	public IJ_DWT() {
		super("Wavelet Transform");
        }
        
        @Override
	public void run(String arg) {
            JFrame mainFrame = new DwtFrame();
            mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            mainFrame.setVisible(true);
	}

}