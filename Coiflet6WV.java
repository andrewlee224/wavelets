/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public class Coiflet6WV implements WaveletInterface 
{

    public String getName() 
    {
       return "Coiflet-6"; //To change body of generated methods, choose Tools | Templates.
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getLowpassFilterParametres() 
    {
              double[] p = new double[6];
                      p[5] =0.07273261951285;
                      p[4] =0.33789766245781;
                      p[3] =-0.85257202021226;
                      p[2] =0.38486484686420;
                      p[1] =0.07273261951285;
                      p[0] =-0.01565572813546;
                      return p;
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getHighpassFilterParametres() 
    {
               double[] p = new double[6];
                      p[0] =-0.07273261951285;
                      p[1] =0.33789766245781;
                      p[2] =0.85257202021226;
                      p[3] =0.38486484686420;
                      p[4] =-0.07273261951285;
                      p[5] =-0.01565572813546;
                      return p;
    }
    
    public int getOrder() 
    {
       return 2;
    }

    public int getNumberOfParametres() 
    {
        return 6;
    }

}
