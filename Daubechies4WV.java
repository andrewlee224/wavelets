/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public class Daubechies4WV implements WaveletInterface 
{

    public String getName() 
    {
       return "Daubechies-4"; //To change body of generated methods, choose Tools | Templates.
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getLowpassFilterParametres() 
    {
              double[] p = new double[4];
                      p[3] =(-1-Math.sqrt(3))/(4*Math.sqrt(2));
                      p[2] =(3+Math.sqrt(3))/(4*Math.sqrt(2));
                      p[1] =(-3+Math.sqrt(3))/(4*Math.sqrt(2));
                      p[0] =(1-Math.sqrt(3))/(4*Math.sqrt(2));
                      return p;
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getHighpassFilterParametres() 
    {
               double[] p = new double[4];
		      p[0] =(1+Math.sqrt(3))/(4*Math.sqrt(2));
                      p[1] =(3+Math.sqrt(3))/(4*Math.sqrt(2));
                      p[2] =(3-Math.sqrt(3))/(4*Math.sqrt(2));
                      p[3] =(1-Math.sqrt(3))/(4*Math.sqrt(2));
                      return p;
    }
    
    public int getOrder() 
    {
       return 2;
    }

    public int getNumberOfParametres() 
    {
        return 4;
    }

}
