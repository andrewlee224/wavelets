/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DWT;

/**
 *
 * @author Whisraper
 */
public class Coiflet12WV implements WaveletInterface 
{

    public String getName() 
    {
       return "Coiflet-12"; //To change body of generated methods, choose Tools | Templates.
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getLowpassFilterParametres() 
    {
              double[] p = new double[12];
                      p[11] =-0.016387336463;
                      p[10] =-0.041464936781;
                      p[9] =0.067372554722;
                      p[8] =0.386110066823;
                      p[7] =-0.812723635449;
                      p[6] =0.417005184423;
                      p[5] =0.076488599078;
                      p[4] =-0.059434418646;
                      p[3] =-0.023680171946;
                      p[2] =0.005611434819;
                      p[1] =0.001823208870;
                      p[0] =-0.000720549446;
                      return p;
    }
    //-----zmieniono wsp�czynniki falki na te z ksi��ki-----
    public double[] getHighpassFilterParametres() 
    {
               double[] p = new double[12];
                      p[0] =0.016387336463;
                      p[1] =-0.041464936781;
                      p[2] =-0.067372554722;
                      p[3] =0.386110066823;
                      p[4] =0.812723635449;
                      p[5] =0.417005184423;
                      p[6] =-0.076488599078;
                      p[7] =-0.059434418646;
                      p[8] =0.023680171946;
                      p[9] =0.005611434819;
                      p[10] =-0.001823208870;
                      p[11] =-0.000720549446;
                      return p;
    }
    
    public int getOrder() 
    {
       return 4;
    }

    public int getNumberOfParametres() 
    {
        return 12;
    }

}
