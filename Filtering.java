/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DWT;
import java.util.ArrayList;

/**
 *
 * @author Whisraper
 */
public class Filtering 
{
    WaveletInterface wv;
    double[][] original_img;
    double[][] changed_img;
    int maxlevel;
    int width;
    int height;
    int size;
    ArrayList<Integer> xmax_list;
    ArrayList<Integer> ymax_list;

    public Filtering(String waveletType , short[][] img, int maxlevel, int width, int height, int size) 
    {
        wv = null; //falka
        if (waveletType.equals("Haare")) wv = new HaareWV();
        else if (waveletType.equals("Daubechies 4")) wv = new Daubechies4WV();
        else if (waveletType.equals("Daubechies 8"))  wv = new Daubechies8WV();
        else if (waveletType.equals("Coiflet 6")) wv = new Coiflet6WV();
        else if (waveletType.equals("Coiflet 12")) wv = new Coiflet12WV();
        this.maxlevel=maxlevel; //stopie� dekompozycji
        this.width = width;
        this.height = height;
        this.size=size;
        this.original_img=new double[size][width*height];
        this.changed_img=new double[size][width*height];
        for (int i=0; i<size; i++)
        {
            for(int j=0; j<width*height; j++)
            {
                this.original_img[i][j] = (double) img[i][j]; //kopia orginalnego obrazeka
                this.changed_img[i][j] = (double) img[i][j]; //zmieniany obrazek
            }
        }
        this.xmax_list = new ArrayList<Integer>(); //lista wsp��dnych x kolejnych poziom�w dekompozycji
        this.ymax_list = new ArrayList<Integer>(); //lista wsp��dnych y kolejnych poziom�w dekompozycji
    }
    //-----dekompozycja (przepisano z ksi��ki i poprawiono wyst�puj�cy tam b��d)-----
    public void decompose (double[] xdata, int N)
    {
        double[] ydata;
        ydata = new double [N];
        for (int i=0; i<N; i++) ydata[i]=0;
        int s=N/2;
        int j=0;
        int didx;
        for (int i=0; i<N; i=i+2)
        {
            for (int k=0; k<wv.getNumberOfParametres(); k++)
            {
                didx=(i+k)%N;
                ydata[j]=ydata[j]+(wv.getHighpassFilterParametres())[k]*xdata[didx];
                ydata[j+s]=ydata[j+s]+(wv.getLowpassFilterParametres())[k]*xdata[didx];  
            }
            j++;
        }
        System.arraycopy(ydata, 0, xdata, 0, N);
    }
    //-----rekonstrukcja (przepisano z ksi��ki)-----
    public void reconstruct (double[] xdata, int N)
    {
        double[] ydata;
        ydata = new double [N];
        for (int i=0; i<N; i++) ydata[i]=0;
        int s=N/2;
        int j=0;
        int didx;
        for (int i=0; i<N; i=i+2)
        {
            for (int k=0; k<wv.getNumberOfParametres(); k++)
            {
                didx=(i+k)%N;
                ydata[didx]=ydata[didx]+(wv.getLowpassFilterParametres())[k]*xdata[j];
                ydata[didx]=ydata[didx]+(wv.getHighpassFilterParametres())[k]*xdata[j+s];  
            }
            j++;
        }
        System.arraycopy(ydata, 0, xdata, 0, N);
    }
    //-----interpolacja-----
    public void interpolate (double[] xdata, int N, int M)
    {
        //g�ra
        for (int x=1; x<N-1; x++) if (x%2==1) xdata[x]=(xdata[x+1]+xdata[x-1]+xdata[width+x])/3;
        //do�
        for (int x=1; x<N-1; x++) if ((M-1+x)%2==1) xdata[width*(M-1)+x]=(xdata[width*(M-1)+x+1]+xdata[width*(M-1)+x-1]+xdata[width*(M-2)+x])/3;
        //lewa
        for (int y=1; y<M-1; y++) if (y%2==1) xdata[width*y]=(xdata[width*(y+1)]+xdata[width*(y-1)]+xdata[width*y+1])/3;
        //prawa
        for (int y=1; y<M-1; y++) if ((y+N-1)%2==1) xdata[width*y+N-1]=(xdata[width*(y+1)+N-1]+xdata[width*(y-1)+N-1]+xdata[width*y+N-2])/3;
        //centrum
        for (int y=1; y<M-1; y++) for (int x=1; x<N-1; x++) if ((y+x)%2==1) xdata[width*y+x]=(xdata[width*(y+1)+x]+xdata[width*(y-1)+x]+xdata[width*y+x+1]+xdata[width*y+x-1])/4;
    }
    //-----twarde progowanie-----
    public double hthresh(double x, double t)
    {
        if (Math.abs(x)>t) return x;
        else return 0;
    }
    //-----dwuwymiarowa transformata falkowa (przepisano z ksi��ki)-----
    public void dwt_2D ()
    {
        int xmax;
        int ymax;
        int level;
        for (int i=0;i<size;i++)
        {
            xmax=width;
            ymax=height;
            level=0;
            while ((xmax>=4 && ymax>=4 && (level<maxlevel)))
            {
                double[] row;
                row = new double[xmax];
                for (int y=0; y<ymax; y++)
                {
                    System.arraycopy(changed_img[i], width*y, row, 0, xmax);
                    decompose (row, xmax);
                    System.arraycopy(row, 0, changed_img[i], width*y, xmax);
                }
                double[] column;
                column = new double[ymax];
                for (int x=0; x<xmax; x++)
                {
                    for (int y=0; y<ymax; y++) column[y]=changed_img[i][width*y+x];
                    decompose (column, ymax);
                    for (int y=0; y<ymax; y++) changed_img[i][width*y+x]=column[y];
                }
                if (i==0)
                {
                    xmax_list.add(xmax);
                    ymax_list.add(ymax);
                }
                xmax/=2;
                ymax/=2;
                level++;
            }
        }
    }
    //-----dwuwymiarowa odwrotna transformata falkowa (nie dzia�a prawid�owo)-----
    public void idwt_2D ()
    {
        int xmax;
        int ymax;
        for (int i=0;i<size;i++)
        {
            for (int level=xmax_list.size()-1; level>=0; level--)
            {
                xmax=xmax_list.get(level);
                ymax=ymax_list.get(level);
                double[] row;
                row = new double[xmax];
                for (int y=0; y<ymax; y++)
                {
                    System.arraycopy(changed_img[i], width*y, row, 0, xmax);
                    reconstruct (row, xmax);
                    System.arraycopy(row, 0, changed_img[i], width*y, xmax);
                }
                double[] column;
                column = new double[ymax];
                for (int x=0; x<xmax; x++)
                {
                    for (int y=0; y<ymax; y++) column[y]=changed_img[i][width*y+x];
                    reconstruct (column, ymax);
                    for (int y=0; y<ymax; y++) changed_img[i][width*y+x]=column[y];
                }
                interpolate (changed_img[i], xmax, ymax);
            }
        }
    }
    //-----w�a�ciwa filtracja (przepisano z ksi��ki)-----
    public void filtering ()
    {
        int xmax; 
        int ymax;
        double uthresh;
        double gcvmin;
        double optthresh;
        double gcv;
        int cnt;
        double a;
        for (int i=0;i<size;i++)
        {
            for (int level=1; level<xmax_list.size(); level++)
            {
                xmax=xmax_list.get(level);
                ymax=ymax_list.get(level);
                gcvmin=1e20;
                optthresh=0;
                uthresh=0;
                for (int y=0; y<height; y++) for (int x=0; x<width; x++) if ((x>=xmax || y>=ymax) && uthresh<Math.abs(changed_img[i][width*y+x])) uthresh=Math.abs(changed_img[i][width*y+x]);
                for (double t=0; t<uthresh; t=t+uthresh/200)
                {
                    gcv=0;
                    cnt=0;
                    for (int y=0; y<height; y++) for (int x=0; x<width; x++) if (x>=xmax || y>=ymax)
                    {
                        a=hthresh(changed_img[i][width*y+x],t);
                        gcv=gcv+(a-changed_img[i][width*y+x])*(a-changed_img[i][width*y+x]);
                        if (a==0) cnt++;
                    }
                    if (cnt>0)
                    {
                        gcv=gcv/(cnt*cnt);
                        if (gcv<gcvmin)
                        {
                            gcvmin=gcv;
                            optthresh=t;
                        }
                    }
                }
                for (int y=0; y<height; y++) for (int x=0; x<width; x++) if (x>=xmax || y>=ymax) changed_img[i][width*y+x]=hthresh(changed_img[i][width*y+x],optthresh);
            }
        }
    }
    
    public short [] get_original_pixels(int layer)
    {
        short [] pixels = new short [height*width];
        for (int i=0; i<height*width; i++) pixels[i]=(short) original_img[layer][i];
        return pixels;
    }
    
    public short [] get_changed_pixels(int layer)
    {
        short [] pixels = new short [height*width];
        for (int i=0; i<height*width; i++) pixels[i]=(short) changed_img[layer][i];
        return pixels;
    }
}
